#include <stdio.h>
int F(int n);

main() {
	int n;
	int result;
	

	scanf("%d",&n);
	
	result = F(n);
	printf("%d\n",result);
}


int F(int n){

	int temp1 = 0;
	int temp2 = 1;
	int temp3;
	
	if( n == 0 ){
		return temp1;
	} else if ( n == 1 ){
		return temp2;
	} else {
		for( int i = 2; i <= n; i++){
			temp3 = temp2 + temp1;
			temp1 = temp2;
			temp2 = temp3;
		}
		return temp3;
	} 
				
}
