#include <iostream>
#include <cmath>
#include <assert.h>
#include "simpson/integrate_simpson.hpp"
#include <fstream>
using namespace std;

double fct( double x );

int main(int argc, char** argv){
	int N;
	double a = 0;
	double b = M_PI;
	double solution;

	for( int N = 1; N < 30; N++){
		integrate_simpson(&solution, a, b, N);
		ofstream ofs ("test.txt", ofstream::app);
		ofs << N << " " << solution << "\n";
		ofs.close();
	}

}


	

