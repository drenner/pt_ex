add_library(integrate_simpson STATIC integrate_simpson.cpp)
target_include_directories(integrate_simpson PUBLIC ./)

install(TARGETS integrate_simpson DESTINATION lib)
install(FILES integrate_simpson.hpp DESTINATION include)
