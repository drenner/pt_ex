# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/diego/Desktop/Uni/PT/pt_ex/03/the-simpson-collaboration/integrate.cpp" "/home/diego/Desktop/Uni/PT/pt_ex/03/the-simpson-collaboration/build/CMakeFiles/integrate.dir/integrate.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../simpson/."
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/diego/Desktop/Uni/PT/pt_ex/03/the-simpson-collaboration/build/simpson/CMakeFiles/integrate_simpson.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
