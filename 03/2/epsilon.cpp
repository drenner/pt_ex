#include <iostream>
#include <limits>

template <class myType> myType halfing ();
template <class myType> myType averaging ();

int main (int argc, char **argv){
    std::cout << "halfing<float> "<< halfing<float>() << "\n";
    std::cout << "halfing<double> "<< halfing<double>() << "\n";
    std::cout << "halfing<long double> "<< halfing<long double>() << "\n";
    std::cout << "averaging<float> "<< averaging<float>() << "\n";
    std::cout << "averaging<double> "<< averaging<double>() << "\n";
    std::cout << "averaging<long double> "<< averaging<long double>() << "\n";
    std::cout << "std::numeric_limits<float> "<< std::numeric_limits<float>::epsilon() << "\n";
    std::cout << "std::numeric_limits<double> "<< std::numeric_limits<double>::epsilon() << "\n";
    std::cout << "std::numeric_limits<long double> "<< std::numeric_limits<long double>::epsilon() << "\n";
}


template <class myType>
myType halfing () {
    myType result;
    result = 1;
    while( result+1 != 1){
        result /= 2;
    }
    return result*2;
}

template <class myType>
myType averaging () {
    myType lower = 0;
    myType upper = 1;
    myType average = (lower+upper)/2;
    myType average_old = 1;
    while( average_old != average){
        average_old = average;
        if( average + 1 != 1 ){
            upper = average;
        } else {
            lower = average;
        }
        average = (upper + lower)/2;
    }
    return average;

}
