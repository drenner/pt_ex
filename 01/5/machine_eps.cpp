#include <iostream>

using namespace std;

double get_eps();

int main(int argc, char** argv){
	double eps;
	eps = get_eps();
	cout << eps << endl;
}

double get_eps(){

	double eps = 1;
	
	do {
		eps /= 2;
	} while ( 1.0 + eps != 1.0);
	
	return eps;
}
	
