#include <iostream>
#include <cmath>

using namespace std;

double fct( double x );
double integrate_simpsons(double a, double b, int N);

int main(int argc, char** argv){
	int N;
	double a;
	double b;
	printf("Enter start and end point as well as number of bins.");
	scanf("%lf %lf %d",&a,&b,&N); 
	
	double solution = integrate_simpsons(a, b, N);
	cout.precision(15);
	cout << solution << endl;

}

double integrate_simpsons(double a, double b, int N){

	double delta = (b-a)/(2*N);
	double solution = fct(a) + fct(b);
	for( int i = 1; i < 2*N; i++){
		solution +=  2*((i%2)+1) * fct( a + i*delta);
	}
	
	return solution/3*delta;
}

double fct( double x ){
	return sin(x);
}
	

