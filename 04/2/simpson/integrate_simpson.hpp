
template<class F , class T> void integrate_simpson(T *solution, T a, T b, int N, F function){

        assert(b >= a && "end point has to be bigger than start point");
        assert(N > 0 && "# of bins has to be bigger than 0");

        double delta = (b-a)/(2*N);
        *solution = function(a) + function(b);
        for( int i = 1; i < 2*N; i++){
                *solution +=  2*((i%2)+1) * function( a + i*delta);
        }
        *solution  = *solution*delta/3;

}


