#include <iostream>
#include <cmath>
#include <assert.h>
#include "simpson/integrate_simpson.hpp"
#include <fstream>
using namespace std;

double sinus( double x ){
    return sin(x);
}    

struct SinusObj{
    double lamda;
    SinusObj(double lambda_value=1.0) : lamda(lambda_value) {}   //look, a constructor
    double operator()(double x) {return sin(x);}
};

int main(int argc, char** argv){
	int N;
	double a = 0;
	double b = M_PI;
	double solution;

        //std::function<double> sinus_obj = [](const double x) { return sin(x); };
        SinusObj sinus_obj(1);
	for( int N = 1; N < 30; N++){
		integrate_simpson(&solution, a, b, N, sinus_obj);
		ofstream ofs ("test.txt", ofstream::app);
		ofs << N << " " << solution << "\n";
		ofs.close();
	}

}



