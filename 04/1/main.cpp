#include <iostream>

enum Z2 {Plus, Minus};

Z2 operator*( Z2 a, Z2 b);
std::ostream& operator<<(std::ostream& os , Z2 a );
template<class T> T operator *( T a , Z2 b );
template<class T> T operator *( Z2 b , T a );
template<class T> T identity_element();
template<> Z2 identity_element();
template<class T> T mypow(T a , unsigned int n );


int main(int argc, char **argv){
    Z2 p = Plus*Minus;
    double a = 5.12312*p;
    std::cout << p << " " << identity_element<Z2>() << " " << mypow(p,4) << "\n";
    return 0;
}

Z2 operator*( Z2 a, Z2 b){
    if ( (a==Minus&&b==Minus)||(a==Plus&&b==Plus)){
        Z2 r = Plus;
        return r;
    } else {
        Z2 r = Minus;
        return r;
    }
}

std::ostream& operator<<(std::ostream& os , Z2 a ){
    if( a==Minus){
        os << "Minus";
        return os;
    } else {
        os << "Plus";
        return os;
    }
}

template<class T> T operator *( T a , Z2 b ){
    if(b==Minus){
        return -a;
    } else{
        return a;
    }
}
template<class T> T operator *( Z2 b , T a ){
    if(b==Minus){
        return -a;
    } else{
        return a;
    }
}

template<class T> T identity_element() { 
    return T(1); 
}

template<> Z2 identity_element(){
    return Plus;
}


template<class T> T mypow(T a , unsigned int n ){
    
    T res = identity_element<T>();
    while( n != 0 ){
        if( n%2 != 0 ){
            res = res*a;
        }

        n /= 2;
        a = a*a;
    
    }
    return res;
}
