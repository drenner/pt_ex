#include <stdio.h>



int main( int argc, char** argv){

	int n_max = 3;
	int n;
	double array[n_max];
	double sum;
	scanf("%d", &n);
	
	for( int i = 0; i < n; i++ ){
		scanf("%lf", &array[i]);
		sum += array[i];
	}

	for( int i = 0; i < n; i++ ){
		array[i] = array[i]/sum;
	}
	

	for( int i = n-1; i >= 0; i-- ){
		printf("%lf\n", array[i]);
	}

}	
	

