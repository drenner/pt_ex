#include <stdio.h>



int main( int argc, char** argv){

	int n;
	double sum;
	scanf("%d", &n);
	double array[n];
	for( int i = 0; i < n; i++ ){
		scanf("%lf", &array[i]);
		sum += array[i];
	}

	for( int i = 0; i < n; i++ ){
		array[i] = array[i]/sum;
	}
	

	for( int i = n-1; i >= 0; i-- ){
		printf("%lf\n", array[i]);
	}

}	
	

