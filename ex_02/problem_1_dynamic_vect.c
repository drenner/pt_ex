#include <stdio.h>
#include <vector>


int main( int argc, char** argv){

	int n;
	double sum;
	double temp;
	scanf("%d", &n);
	std::vector<double> array;
	for( int i = 0; i < n; i++ ){
		scanf("%lf", &temp);
		array.push_back(temp);
		sum += array[i];
	}

	for( int i = 0; i < n; i++ ){
		array[i] = array[i]/sum;
	}
	

	for( int i = n-1; i >= 0; i-- ){
		printf("%lf\n", array[i]);
	}

}	
	

