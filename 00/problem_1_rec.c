#include <stdio.h>
int gcd(int a, int b);

main() {
	int a;
	int b;
	int result;
	

	scanf("%d",&a);
	scanf("%d",&b);
	
	result = gcd(a,b);
	printf("%d\n",result);
}


int gcd(int a, int b){
	int mod;
	int max;
	int min;
	if( a > b ){
		max = a;
		min = b;
	} else {
		max = b;
		min = a;
	}
	
	mod = max%min;
	if( mod != 0 ){
		min = gcd(mod,min);
	}
	return min; 		
}
