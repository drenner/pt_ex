#include <stdio.h>
#include <math.h>

int F(int n);
void make_seq(double *seq, int n);
void sort(double *seq, int n);

main() {
	int n;

	scanf("%d",&n);
	
	double seq[n];
	make_seq(seq, n);
	sort(seq,n);
	printf("\n");
	for( int i = 0; i < n; i++){
		printf("%lf\n", seq[i]);
	}
	//result = F(n);
	//printf("%d\n",result);
}

void make_seq(double *seq, int n){
	for( int i = 0; i < n; i++){
		seq[i] = sin((double)i);
		printf("%lf\n", seq[i]);
	}
} 

void sort(double *seq, int n){
	if( n > 1 ){
		int m = n/2;
		int l = n-m;
		double seq1[m];
		double seq2[l];
		for( int i = 0; i < m; i++){
			seq1[i] = seq[i];
		}
		for( int i = 0; i < l; i++){
			seq2[i] = seq[m+i];
		}

		sort(seq1, m);
		sort(seq2, l);
		int c_1 = 0;
		int c_2 = 0;
		while( c_1 < m && c_2 < l){
		
			if(seq1[c_1] > seq2[c_2] ){
				seq[c_1+c_2] = seq2[c_2];
				c_2 += 1;
			} else {
				seq[c_1+c_2] = seq1[c_1];
				c_1 += 1;
			}
		}
		if( c_1 == m ){
			for( c_2; c_2 < l; c_2++){
				seq[c_1+c_2] = seq2[c_2];
			}
		} else {
			for( c_1; c_1 < m; c_1++){
				seq[c_1+c_2] = seq1[c_1];
			}
		}
	}
}
				 	
		

